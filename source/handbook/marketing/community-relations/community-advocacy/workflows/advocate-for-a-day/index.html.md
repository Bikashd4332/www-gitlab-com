---
layout: handbook-page-toc
title: "Advocate for a Day Workflow"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

When community advocates aren't available, or we expect high traffic on social media (because of some major outage, or some significant announcement), we should try to recruit more GitLab team-members who would help us cover our social networks.

Our job is to make sure all community comments get attention, and that they receive quick, accurate responses. It is not our job to answer every question ourselves. If you see a comment that you don't know the answer to, refer to our [involving experts workflow](/handbook/marketing/community-relations/community-advocacy/workflows/involving-experts/). If they question why they were pinged about this, you can link them to our [Can You Please Respond To This](/handbook/marketing/community-relations/community-advocacy/#can-you-please-respond-to-this) section in the handbook.

## Workflow

### Slack Channels

Please join the following Slack channels:
- [community-advocates](https://gitlab.slack.com/messages/community-advocates)
- [hn-mentions](https://gitlab.slack.com/messages/hn-mentions) 
- (for releases) [release-post](https://gitlab.slack.com/messages/release-post)
- (for releases) [mentions-of-gitlab](https://gitlab.slack.com/messages/mentions-of-gitlab)

#### Hacker News

Monitoring Hacker News is typically our most important duty. Often during releases or major news events, we will have a story trending that needs attention. Please review our [Hacker News workflow](/handbook/marketing/community-relations/community-advocacy/workflows/hackernews).

If we do have a story trending, it's better to manually monitor the story instead of waiting for new comments to come in to #hn-mentions. Comments will only get posted to #hn-mentions if they contain the word `gitlab`, so there may be comments that are missed by only relying on the Slack channel.

### Twitter

Use Tweetdeck to monitor Twitter mentions. If you do not have Tweetdeck access, there is a note in 1Password with a list of people who can give you access. Since advocates use Zendesk instead of Tweetdeck, please post in the #community-advocates channel that you will be helping with Twitter avoid duplicate responses.

Review our [Twitter workflow](/handbook/marketing/community-relations/community-advocacy/workflows/twitter/) here. Note that retweeting is not a part of the advocate's duty. If you feel like something should be retweeted, please inform the content team.

#### Blog Post Comments

The #mentions-of-gitlab channel will post any comments received on our blog posts. During releases, we may see comments on the the release post. You can either monitor the post manually, or use the Slack channel. If you see a comment that needs attention, forward it to someone who can answer


## Useful Links

- [Involving Experts](/handbook/marketing/community-relations/community-advocacy/#involving-experts)
- [Knowledge Base](/handbook/marketing/community-relations/community-advocacy/workflows/knowledge-base/)
- [Social Media Guidelines](/handbook/marketing/social-media-guidelines/)
- ["Where should you look when you need help?" on the Product handbook](/handbook/product/product-management/process/#where-should-you-look-when-you-need-help)
- [See which release manager is currently on duty](/community/release-managers/)
- [List of PMMs by category](/handbook/product/categories/#dev-section)

