---
layout: handbook-page-toc
title: Growth Section
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## FY2021 Direction

Develop a respectful and privacy focussed data collection framework that allows us to make informed product decisions which improve the way our customers sign up to GitLab, manage and renew their licenses, and upgrade their accounts to higher tiers.

## Mission

The Growth section consists of groups that eliminate barriers between our users and our product value.

**Managing customer licensing and transactions**

The [Fulfillment](/handbook/engineering/development/growth/fulfillment/) Group is responsible for:

Licensing
- Seamless license compliance for customers and GitLab Team Members

Transactions
- Make doing business with GitLab  efficient, easy, and a terrific user experience

**Deliver telemetry data that improves our product**

The [Telemetry](/handbook/engineering/development/growth/acquisition-conversion-be-telemetry/) Group is responsible for:

Collection
- Collect information from GitLab.com and self-managed instances to improve our product
- Respect our users' privacy

Analysis
- Analyze and visualize collected data
- Operationalize what we’re collecting for the benefit of GitLab and our customers


**Drive value for the business and our users by improving activation, retention, upsell, and per-stage adoption**

We focus on validating ideas with data across the following four Groups:

[Acquisition Group](/handbook/engineering/development/growth/acquisition-conversion-be-telemetry/)

- Get users off to a successful start
- Increase users completing key actions

[Conversion Group](/handbook/engineering/development/growth/acquisition-conversion-be-telemetry/)
- Encourage cross-stage usage
- Increase Stage Monthly Active Users

[Expansion Group](/handbook/engineering/development/growth/expansion/)
- Drive adoption of “higher” tiers
- Increase revenue per user/transaction

[Retention Group](/handbook/engineering/development/growth/retention/)
- Get customers and users to return
- Increase gross retention

We work closely with the [Data Team](/handbook/business-ops/data-team/) along with our [Product Team](/handbook/product/categories/#growth-stage)
counterparts to design and implement experiments that measure the impact of changes to our messaging, UX, and overall experience of using GitLab.

## Section Members

The following people are permanent members of the Growth Section:

<%=
departments = ['Acquisition', 'Conversion', 'Expansion' , 'Retention', 'Fulfillment', 'Telemetry', 'Static Site Editor']
department_regexp = /(#{Regexp.union(departments)})/

direct_team(role_regexp: department_regexp, manager_role: 'Director of Engineering, Growth')
%>

## All Team Members

The following people are permanent members of groups that belong to the Growth Section:

### Acquisition
<%= department_team(base_department: "Acquisition Team") %>

### Conversion
<%= department_team(base_department: "Conversion Team") %>

### Expansion
<%= department_team(base_department: "Expansion Team") %>

### Retention
<%= department_team(base_department: "Retention Team") %>

### Fulfillment Backend
<%= department_team(base_department: "Fulfillment BE Team") %>

### Fulfillment Frontend
<%= department_team(base_department: "Fulfillment FE Team") %>

### Telemetry Backend
<%= department_team(base_department: "Telemetry BE Team") %>

### Telemetry Frontend
<%= department_team(base_department: "Telemetry FE Team") %>


## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%=
role_regexp = /[,&] (Growth|Fulfillment|Telemetry)/
direct_manager_role = 'Director of Engineering, Growth'
other_manager_roles = [
  'Frontend Engineering Manager, Fulfillment and Telemetry',
  'Engineering Manager, Growth:Acquisition and Conversion and Telemetry',
  'Engineering Manager, Growth:Expansion and Retention',
  'Engineering Manager, Fulfillment'
]
stable_counterparts(role_regexp: role_regexp, direct_manager_role: direct_manager_role, other_manager_roles: other_manager_roles)
%>

## How We Work
As part of the wider Growth stage we track and work on issues with the label `~"devops::growth"`.

### Product Development Flow

Our team follows the [Product Development Flow](https://about.gitlab.com/handbook/product-development-flow/#workflow-summary) utilizing all labels from `~workflow::start` to `~workflow::verification`. 

We adhere to the **Completion Criteria** and **Who Transitions Out** outlined in the [Product Development Flow](https://about.gitlab.com/handbook/product-development-flow/#workflow-summary) to progress issues from one stage to the next. 

#### Workflow Boards

We use workflow boards to track issue progress throughout a milestone. Workflow boards should be viewed at the highest group level for visibility into all nested projects in a group. 

There are three GitLab groups we use: 
- The [gitlab.com/gitlab-org](https://gitlab.com/gitlab-org/) group includes the [gitlab](https://gitlab.com/gitlab-org/gitlab), [customers-gitlab-com](https://gitlab.com/gitlab-org/customers-gitlab-com), and [license-gitlab-com](https://gitlab.com/gitlab-org/license-gitlab-com) projects.
- The [gitlab.com/gitlab-com](https://gitlab.com/gitlab-com/) group includes the [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com) project.
- The [gitlab.com/gitlab-services](https://gitlab.com/gitlab-services/) group includes the [version-gitlab-com](https://gitlab.com/gitlab-services/version-gitlab-com) project.

| gitlab-org | gitlab-com | gitlab-services | all groups | 
| ------ | ------ | ------ | ------ |
| [Growth Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1546862?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth) | [Growth Workflow](https://gitlab.com/groups/gitlab-services/-/boards/1546865?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth) | [-](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth) |
| [Acquisition Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aacquisition) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1546862?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aacquisition) | [Acquisition Workflow](https://gitlab.com/groups/gitlab-services/-/boards/1546865?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aacquisition) | [-](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aacquisition) |
| [Conversion Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1546862?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion) | [Conversion Workflow](https://gitlab.com/groups/gitlab-services/-/boards/1546865?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion) | [-](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion) |
| [Expansion Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1546862?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion) | [Expansion Workflow](https://gitlab.com/groups/gitlab-services/-/boards/1546865?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion) | [-](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion) |
| [Retention Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aretention) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1546862?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aretention) | [Retention Workflow](https://gitlab.com/groups/gitlab-services/-/boards/1546865?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aretention) | [-](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aretention) |
| [Fulfillment Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Afulfillment) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1546862?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Afulfillment) | [-](https://gitlab.com/groups/gitlab-services/-/boards/1546865?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Afulfillment) | [-](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Afulfillment) |
| [Telemetry Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Atelemetry) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1546862?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Atelemetry) | [Telemetry Workflow](https://gitlab.com/groups/gitlab-services/-/boards/1546865?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Atelemetry) | [-](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Atelemetry) |

### Product Development Timeline

Our work is planned and delivered on a monthly cycle using [milestones](https://docs.gitlab.com/ee/user/project/milestones/). Our team follows the [Product Development Timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline) utilizing all dates including from `M-1, 4th: Draft of the issues` to `M+1, 4th: Public Retrospective`.

#### Milestone Boards

We use milestone boards for high level planning and roadmapping across several milestones.

| gitlab-org | gitlab-com | gitlab-services |
| ------ | ------ | ------ |
| [Growth Milestones](https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1547281?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth) | [Growth Milestones](https://gitlab.com/groups/gitlab-services/-/boards/1547332?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth) |
| [Acquisition Milestones](https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aacquisition) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1547281?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aacquisition) | [Acquisition Milestones](https://gitlab.com/groups/gitlab-services/-/boards/1547332?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aacquisition) |
| [Conversion Milestones](https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1547281?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion) | [Conversion Milestones](https://gitlab.com/groups/gitlab-services/-/boards/1547332?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion) |
| [Expansion Milestones](https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1547281?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion) | [Expansion Milestones](https://gitlab.com/groups/gitlab-services/-/boards/1547332?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion) |
| [Retention Milestones](https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aretention) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1547281?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aretention) | [Retention Milestones](https://gitlab.com/groups/gitlab-services/-/boards/1547332?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aretention) |
| [Fulfillment Milestones](https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Afulfillment) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1547281?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Afulfillment) | [-](https://gitlab.com/groups/gitlab-services/-/boards/1547332?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Afulfillment) |
| [Telemetry Milestones](https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Atelemetry) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1547281?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Atelemetry) | [Telemetry Milestones](https://gitlab.com/groups/gitlab-services/-/boards/1547332?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Atelemetry) |

### UX Workflows
We follow the UX Team's [Product Designer](/handbook/engineering/ux/ux-designer/#product-designer)
and [Researcher](/handbook/engineering/ux/ux-research/) workflows.
We also have Growth specific workflows that you can read about [here](/handbook/engineering/ux/stage-group-ux-strategy/growth/#how-we-work).

#### Sharing Designers Across Stage Groups

To start with, we follow [GitLab internal communication](https://about.gitlab.com/handbook/communication/#internal-communication) guidelines. In addition the following tips will make it easier to collaborate with Product Designers who span multiple groups:

* Overcommunication is better than undercommunication, especially in this case. 
    * Communicate priorities clearly and transparently. Communicate UX priorities via  priority labels (ie ~"milestone::p1"), due dates and issue boards instead of in 1:1 docs or Slack channels. The PMs from both covered stage groups, all the product designers and the UX manager need to be able to access those priorities.
    * When you need a designers immediate attention, don't hesitate to ping them in more than one channel (GitLab + Slack + email) and/or more than one time. You might feel like you are being annoying, but remember that the designer has to juggle a couple task lists with separate priorities. It can be easy to miss something. 
* Include the UX Manager more than you normally would, because the UX Manager can help balance priorities or address situations where one designer has an overload of work to do.
* On our monthly planning issues, each designer should indicate a couple of extra items: 
    * An estimate of how their time will be split (e.g. 40% Fulfillment, 50% Retention, 10% Design System)
    * Links to the high priority or time consuming issues from other stage groups. A link to the other planning issue is fine too, just so it's easy for the UX Manager and Product Managers to navigate to this information.
* Minimize meetings: Designers who span stage groups have 2-3 times more meetings than a designer on 1 stage group, so their teams can help them by ensuring we follow GitLab's async meeting practices to allow them to miss those meetings without negative consequences.


#### Visual Reviews of MRs

The engineering team applies the `UX` label to any MR that introduces a visual, interaction or flow change. These MRs can be related to new issues, bugs, followups, or any type of MR. If the engineer isn't sure whether the MR needs UX, they should consult the designer who worked on the related issue, and/or the designer assigned to that stage group, or the UX manager.

Visual reviews are required for any MR with the `UX` label. When the MR is in `workflow::In review`, the engineer assigns the MR to the designer for a visual review. This can happen in parallel with the maintainer review, but designers should prioritize these reviews to complete them as quickly as possible.

There are times when it isn't possible or practical for a designer to complete their visual review via Review Apps or GDK. At these times the designer and engineer should coordinate a demo.

### How We Use Issues

To help our team be [efficient](/handbook/values/#efficiency), we explicitly define how our team uses issues.

#### Issue Creation

We aim to create issues in the same project as where the future merge request will live. For example, if an experiment is being run in the Customers application, both the issue and MR should be created in the Customers project. 

We emphasize creating the issue in the right project to avoid having to close and move issues later in the development process. If the location of the future merge request cannot be determined, we will create the issue in our catch-all [growth team-tasks project](https://gitlab.com/gitlab-org/growth/team-tasks/issues).

## Running Experiments

We follow a four step process for running experiments as outlined by [Andrew Chen's How to build a growth team.](https://andrewchen.co/how-to-build-a-growth-team/) 
1. **Form Hypotheses:** Define ideas our team wants to test.
2. **Prioritize Ideas:** Decide which ideas to test first.
3. **Implement Experiments:** Do the Product, Design, Engineering, Data, and Marketing work to execute the experiment. 
4. **Analyze Results:** Dive into the results data and prove or disprove our hypotheses. 

Each week, we provide progress updates and talk about our learnings in our [Growth Weekly Meeting](/handbook/product/growth/#weekly-growth-meeting).

The duration of each experiment will vary depending on how long it takes for experiment results to reach statistical significance. Due to the varying duration, there will be some weeks where we have several experiments running concurrently in parallel.

### Experiment Issue Boards
Experiments are tracked on Growth - Experiments boards by group:

| gitlab-org | gitlab-com | gitlab-services | all groups | 
| ------ | ------ | ------ | ------ |
| [Growth Experiments](https://gitlab.com/groups/gitlab-org/-/boards/1352542) | [Growth Experiments](https://gitlab.com/groups/gitlab-com/-/boards/1542208) | [Growth Experiments](https://gitlab.com/groups/gitlab-services/-/boards/1542265) | [-](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=growth%20experiment) |

### Experiment Issue Creation
All growth experiments consist of two issues:
* **Experiment issue**: this issue acts as the single source of truth for an experiment which includes an experiment overview, the rollout plan, and the results. This issue will be tagged with the `~"growth experiment"` label, and a scoped `experiment::` label.
* **Clean up issue**: this issue is used to clean up an experiment after an experiment is completed. The clean up work may include completely removing the experiment or refactoring the experiment feature for the long run.

### Experiment Issue Labels
For tracking the status of experiments we also use the following scoped `experiment::` labels:
* `~"experiment::active"`
* `~"experiment::blocked"`
* `~"experiment::validated"`
* `~"experiment::invalidated"`
* `~"experiment::inconclusive"`

### Release Schedule

In order to deploy an experiment, experiments need to be tagged with a milestone and aligned to GitLab's [release schedule](https://about.gitlab.com/handbook/engineering/releases/#gitlabcom-releases-1).

Release schedules vary depending on where an experiment is being conducted.
- [GitLab.com:](https://about.gitlab.com/handbook/engineering/releases/#gitlabcom-releases-1) Twice a week
- [GitLab self-managed:](https://about.gitlab.com/handbook/engineering/releases/#self-managed-releases-1) One a month
- [Customers application:](https://gitlab.com/gitlab-org/customers-gitlab-com) Daily

Experiments are generally excluded from [monthly release posts](https://about.gitlab.com/handbook/marketing/blog/release-posts/#contributing-to-a-section) as they are behind feature flags and [usually made available](https://about.gitlab.com/handbook/marketing/blog/release-posts/#feature-availability) on GitLab.com. An experiment feature can be added to the release post only after it has been implemented for the long run. 

## Growth Engineering Weekly

Every week, engineers in the growth section meet to discuss topics related to growth engineering. Discussion topics include how to track experiments, A/B testing, changes in customers application, changes in gitlab application, etc. Growth Engineers are encouraged to bring discussion topics to the meeting and to them to the [agenda](https://docs.google.com/document/d/1VMj16-tvJg4m26y6q7A1jSdBD895ImFM2fbXvFXF4yM/edit?usp=sharing).

To get the most time zone coverage, these meetings alternate fortnightly between:
* Wednesdays 3:00PM UTC for US/EMEA
* Wednesdays 8:00PM UTC for US/APAC

Team members are encouraged to attend the meeting that matches their time zone.

## September 2019 Fast Boot

The Acquisition, Expansion, Conversion and Retention groups took part in a [Fast Boot](/handbook/engineering/fast-boot/) in September 2019.
[The planning issue](https://gitlab.com/gitlab-org/growth/product/issues/1) contains the proposal for Fast Boot,
and outcomes are available in the [Growth Fast Boot Page](/handbook/engineering/development/growth/fast-boot).

## Common Links

* [Growth section]
* [Growth workflow board]
* `#s_growth` in [Slack](https://gitlab.slack.com/archives/s_growth)
* [Growth Performance Indicators]
* [Fulfillment issues board]
* `#g_fulfillment` in [Slack](https://gitlab.slack.com/archives/g_fulfillment)
* [Telemetry issues board]
* `#g_telemetry` in [Slack](https://gitlab.slack.com/archives/g_telemetry)
* [Growth opportunities]
* [Growth meetings and agendas]
* [GitLab values]

[GitLab values]: /handbook/values/
[Growth section]: /handbook/engineering/development/growth/
[Growth workflow board]: https://gitlab.com/groups/gitlab-org/-/boards/1158847
[Growth opportunities]: https://gitlab.com/gitlab-org/growth/product
[Growth meetings and agendas]: https://docs.google.com/document/d/1QB9uVQQFuKhqhPkPwvi48GaibKDwGAfKKqcF-s3Y6og

[Growth Performance Indicators]: /handbook/engineering/development/growth/performance-indicators/

[Fulfillment issues board]: https://gitlab.com/gitlab-org/fulfillment/issues
[Telemetry issues board]: https://gitlab.com/gitlab-org/telemetry/issues
