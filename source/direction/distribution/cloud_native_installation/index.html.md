---
layout: markdown_page
title: "Category Strategy - Cloud Native Installation"
---

- TOC
{:toc}

## Enablement

| | |
| --- | --- |
| Stage | [Enablement](/direction/enablement/) |
| Maturity | [Viable](/direction/maturity/) |
| Content Last Reviewed | `2020-03-06` |

### Introduction and how you can help
Thanks for visiting this category strategy page for the Cloud Native Installation. This page belongs to the [Distribution](/handbook/product/categories/#distribution-group) group of the Enablement stage and is maintained by Larissa Lane ([E-Mail](mailto:<llane@gitlab.com>)).

Your ideas and feedback are important to us and play a major part in shaping the product direction. The best way to provide feedback on scheduled improvements is by commenting on issues in the [deliverable issues board](https://gitlab.com/groups/gitlab-org/-/boards/1282058?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Deliverable&label_name[]=group%3A%3Adistribution). To create a new feature request, create an issue in the [Charts project](https://gitlab.com/gitlab-org/charts/gitlab). 

We are always looking for opportunities to interview GitLab users and learn more about your experiences using the cloud-native install to deploy a self-managed instance of GitLab. If you would like to share your experience and provide feedback throgh a video call, please [email Larissa](mailto:<llane@gitlab.com>).


<!-- ### Overview
A good description of what your category is today or in the near term. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Provide enough context that someone unfamiliar
with the details of the category can understand what is being discussed. -->

<!-- #### Target Audience
List the personas (https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas#user-personas) involved in this category.

Look for differences in user's goals or uses that would affect their use of the product. Separate users and customers into different types based on those differences that make a difference.
-->

<!-- #### Challenges to address
- What needs, goals, or jobs to be done do the users have?
- How do users address these challenges today? What products or work-arounds are utilized?

Provide links to UX Research issues, which validate these problems exist.
-->

<!-- ### Where we are Headed 
Describe the future state for your category. 
- What problems are we intending to solve? 
- How will GitLab uniquely address them?
- What is the resulting benefits and value to users and their organizations?

Use narrative techniques to paint a picture of how the lives of your users will benefit from using this
category once your strategy is at least minimally realized. In order to challenge your level of ambition
(with the goal to make it sufficiently high), link to the current market leaders long-term vision and address how
we plan to displace them. -->

<!-- #### What's Next & Why
This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->

<!-- #### What is Not Planned Right Now
Often it's just as important to talk about what you're not doing as it is to 
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand
the reasoning-->

<!-- #### Maturity Plan
It's important your users know where you're headed next. The maturity plan
section captures this by showing what's required to achieve the next level. The
section should follow this format:

This category is currently at the XXXX maturity level, and our next maturity target is YYYY (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)). 

- Link to maturity epic if you are using one, otherwise list issues with maturity::YYYY labels) -->

<!-- ### User success metrics
- What specific user behaviors are indicate that users are trying these features, and solving their problems?
- How will users discover these features?
-->

<!-- ### Why is this important?
- Why is GitLab building this feature? 
- What impact will it have on the broader devops workflow?
- How confident are we? What is the effort?
-->

<!-- ### Competitive Landscape
The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

<!-- ### Analyst Landscape
What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

<!-- ### Top Customer Success/Sales issue(s)
These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

<!-- ### Top user issue(s)
This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

<!-- ### Top internal customer issue(s)
These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

<!-- ### Top Strategy Item(s)
What's the most important thing to move your strategy forward?-->

