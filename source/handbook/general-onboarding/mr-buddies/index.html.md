---
layout: markdown_page
title: "Merge Request Buddies"
---

Merge Request buddies are available to help other team members who need help with merge requests that will update the GitLab handbook or website. Whether you are learning how to use the GitLab Web IDE, make updates to the handbook and website locally, or need answers to other Git and GitLab questions, Merge Request buddies are here to help.

Note: This role should not be confused with [Merge Request Coach](/job-families/expert/merge-request-coach/). The main goal of a Merge Request Coach is to help
[merge requests from the community](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?label_name[]=Community%20contribution)
get merged into GitLab.


## Find a Merge Request Buddy

Visit the [GitLab Team page](/company/team/) and search for 'Merge Request buddy', or ask on Slack in `#mr-buddies`.

## Become a Merge Request Buddy

If you're comfortable using Git and GitLab and want to help team members troubleshoot problems and accelerate their learning, please follow these steps to indicate your availability as a `Merge Request Buddy`: 

1. Find your entry in the [team.yml](/handbook/git-page-update/#11-add-yourself-to-the-team-page) file.
1. Add `Merge Request buddy` to the `departments` section in your entry (keeping your existing departments):
   ```
   departments:
     - ...
     - Merge Request buddy
   ```
1. Add the following code above the `story` section in your entry:
   ```
     expertise:  |
                 <li><a href="/handbook/general-onboarding/mr-buddies/">Merge Request buddy</a></li>
   ```
1. If you already have an `expertise` section, add the list item portion of the above code:
   ```
                 <li><a href="/handbook/general-onboarding/mr-buddies/">Merge Request buddy</a></li>
   ```
