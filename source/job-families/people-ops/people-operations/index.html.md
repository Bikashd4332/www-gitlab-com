---
layout: job_family_page
title: "People Operations"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## People Operations Specialist

<a id="specialist-requirements"></a>

### Job Grade

The People Operations Specialist is a [grade 6](https://about.gitlab.com/handbook/people-group/global-compensation/#gitlab-job-grades).

### Responsibilities

- Provide backup / guidance to the People Experience team in responding to emails in the people operations email and Slack channel to ensure extraordinary customer service to employees and leaders at GitLab.
- Maintain appropriate level of process, program, and policy knowledge in order to assist team members.
- Intake of questions, complaints and concerns.
- Escalating issues to People Business Partners as needed.
- Administration of the signing of our Code of Conduct (annual signing in February and reporting on this).
- Administer probation period process - liaising with managers, communicating with PBP’s, updating team members. Update probation periods in BambooHR and the handbook.
- Manage full cycle of the relocation process and liaise with other teams as necessary.
- Identify automation opportunities.
- Proactively identify process inefficiencies and inconsistencies and collaborate towards an improved and more productive process that improves the employee and/or manager’s experience.
- Announcing changes and improvements in the #company-announcements Slack channel
- Engagement survey administration & implementation.
- Exit and Stay interviews for Individual Contributors.
- Provide support and cover for Associates including completion of Onboarding issues and Offboarding issues.
- Providing support to the Senior Specialist regarding contract changes and administration.
- Work with the ITOps team and video conferencing vendor to improve the experience for GitLab Team members and the team administering the call.
- Engage with co-employer’s and PEO's on a regular basis, especially during onboarding within those regions.
- Identify gaps during GitLab onboarding and co-employer processes.
- Complete ad-hoc projects, reporting, and tasks.
- Administering PTO Ninja.

### Requirements

- The ability to work autonomously and to drive your own performance & development would be important in this role.
- Prior extensive experience in an HR or People Operations role.
- Clear understanding of HR laws in one or multiple countries where GitLab is active.
- Willingness to work odd hours when needed (for example, to call an embassy in a different continent).
- Excellent written and verbal communication skills.
- Exceptional customer service skills.
- Strong team player who can jump in and support the team on a variety of topics and tasks.
- Enthusiasm for and broad experience with software tools.
- Proven experience quickly learning new software tools.
- Willing to work with git and GitLab whenever possible.
- Willing to make People Operations as open and transparent as possible.
- Proven organizational skills with high attention to detail and the ability to prioritize.
- You share our [values](/handbook/values), and work in accordance with those values.
- The ability to work in a fast paced environment with strong attention to detail is essential.
- High sense of urgency and accuracy.
- Bachelor's degree or 2 years related experience.
- Experience at a growth-stage tech company is preferred.
- Ability to use GitLab

## Senior People Operations Specialist

### Job Grade

The Senior People Operations Specialist is a [grade 7](https://about.gitlab.com/handbook/people-group/global-compensation/#gitlab-job-grades).

### Responsibilities

- Maintain appropriate level of process, program, and policy knowledge in order to assist team members.
- Partner with Legal / Tax on international employment and contractual requirements.
- Handle WBSO grant with the Director of Tax.
- Review new locations and work with PEO’s to update contracts. Update handbook appropriately.
- Data retention tracking and compliance.
- Review anniversary gift feedback on an annual basis and work with the supplier to iterate on the experience and ordering process.
- Proactively identify process inefficiencies and inconsistencies and collaborate towards an improved and more productive process that improves the employee and/or manager’s experience.
- Announcing changes and improvements in the #company-announcements Slack channel
- Work on onboarding people experience, values alignment during onboarding and improving onboarding through continuous iteration.
- Stay interviews for IC’s.
- Prepare separation agreements in partnership with People Business Partners.
- Partner with the People Operations Fullstack Engineer on automation and removing mundane tasks from Onboarding.
- Iterate on the onboarding issue and collaborate with the overall team to keep improving onboarding.
- Work with People Ops and the executive team to establish entities or co-employers in new countries as we scale.
- Work with the People Operations Fullstack Engineer on automation related options for call management and videco conferencing provider.
- Manage Vendor contracts.
- Renewing or ending of contracts and working closely with Procurement on negotiations, and vendor selections.
- Coordinate with Finance on PEO payroll issue.
- Complete ad-hoc projects, reporting, and tasks.
- Collaborating with PTO Ninja on a monthly basis to ensure we continue to iterate and improve on the integration and feedback.

## People Operations Team Lead

### Job Grade

The People Operations Team Lead is a [grade 8](https://about.gitlab.com/handbook/people-group/global-compensation/#gitlab-job-grades).

### Responsibilities

- Coach and mentor PeopleOps team to effectively address team member queries in line with our values.
- Continually audit and monitor compliance with policies and procedures.
- Address any gaps and escalate to Manager, People Operations where required.
- Drive continued automation and efficiency to enhance the employee experience and maintain our efficiency value.
- Announcing changes and improvements in the #company-announcements Slack channel
- Driving a positive employee experience throughout their lifecycle with GitLab.
- Monthly report on trends from Stay Interviews.
- Quarterly report on trends from exit interview data.
- Partner with all relevant teams in GitLab and PEO's on conversions.
- Manage vendor renewals and agreements.
- Partner with Finance Business Partner for budgeting purposes.
- Complete ad-hoc projects, reporting, and tasks.

### Performance Indicators

Primary performance indicators for this role:

- [Onboarding TSAT > 4](/handbook/people-group/people-operations-metrics/#onboarding-enps)
- [Onboarding task completion < X (TBD)](/handbook/people-group/people-operations-metrics/#onboarding-enps)

### Hiring Process
- Qualified candidates will be invited to schedule a 30 minute screening call with one of our recruiters
- Next, candidates will be invited to schedule a 45 minute interview with our People Operations Manager
- After that, candidates will be invited to schedule a 30 minute interview with members of the People Operations and People Partner teams
- After that, candidates will be invited to interview with the Director of People Operations
- Finally, our CPO may choose to conduct a final interview

## Senior Manager, People Operations

Manages the People Experience team, People Operations Specialist team and People Operations Full Stack Engineer team

### Job Grade

The Senior Manager, People Operations is a [grade 8](https://about.gitlab.com/handbook/people-group/global-compensation/#gitlab-job-grades).

### Responsibilities
* Onboard, mentor, and grow the careers of all team members
* Provide coaching to improve performance of team members and drive accountability
* Work with Director, People Operations to shape a strategy that aligns and moves GitLab towards continued growth, innovation and improvement
* Provide training and support to the Specialist group to address team member and leadership queries effectively and timely
* Oversee the co-employer relationships and act as the main point of contact for the People Group
* Partner with Finance and Legal to establish entities or co-employers in new countries
* Seek and review potential blockers of various People Operations processes and ensuring improvement on daily tasks and suggest automation where needed
* Improve Slack answers and continue to train and mentor the team to update information in the handbook, for easy access to all
* Implement an SLA to track response times for email queries across timezones
* Continue to drive automation for easy access of employment confirmation letters, automatic invitations to Contribute, confirmation of business insurance, etc
* Implementation of data retention strategies and partnering with People Leads to ensure this is implemented across all team member data consistently
* Continuous Improvement of both administration and people experience during onboarding.
* Partnering closely with Managers at GitLab to gain feedback from various teams about onboarding, offboarding and transition issues
* Review and report on People Operations and Onboarding metrics, including [OSAT](/handbook/people-group/people-group-metrics/#onboarding-satisfaction-osat) and [Onboarding Task Completion](/handbook/people-group/people-group-metrics/#onboarding-task-completion--x-tbd)
* Continuous improvement of offboarding for both voluntary and involuntary terms
* Along with the People Business Partners, create a Terminations playbook to ensure the team can remain async and scale
* Work closely with payroll, SecOps, and ITOps to continue to improve the administration of offboarding the the time it takes to complete

### Requirements
* Ability to use GitLab
* The ability to work autonomously and to drive your own performance & development would be important in this role
* Prior extensive experience in a People Operations role
* Willingness to work odd hours when needed (for example, to call an embassy in a different continent)
* Excellent written and verbal communication skills
* Exceptional customer service skills
* Strong team player who can jump in and support the team on a variety of topics and tasks
* Enthusiasm for and broad experience with software tools
* Proven experience quickly learning new software tools
* Willing to work with git and GitLab whenever possible
* Willing to make People Operations as open and transparent as possible
* Proven organizational skills with high attention to detail and the ability to prioritize
* You share our values, and work in accordance with those values
* The ability to work in a fast paced environment with strong attention to detail is essential
* High sense of urgency and accuracy
* Experience at a growth-stage tech company

### Performance Indicators
* 12 month team member retention
* 12 month voluntary team member turnover
* Onboarding TSAT > 4
* Onboarding task completion < X (TBD)

### Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Next, candidates will be invited to schedule a 45 minute interview with our People Operations Manager
* After that, candidates will be invited to schedule a 30 minute interview with members of the People Operations and People Partner teams
* After that, candidates will be invited to interview with the Director of People Operations
* Finally, our CPO may choose to conduct a final interview

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Director, Global People Operations

The Director of Global People Operations is responsible for overseeing the employee experience throughout the employee lifecycle. This position will report to the Chief People Officer and work closely with the organizational leaders in shaping the Gitlab cultural strategies to support ongoing innovation and change. This position will use their deep experience in HR areas of labor relations, compensation, benefits, organizational strategy, employee lifecycle activities, and compliance with HR policies and procedures to enable Gitlab's rapid growth.

### Job Grade

The Director, Global People Operations is a [grade 10](https://about.gitlab.com/handbook/people-group/global-compensation/#gitlab-job-grades).

### Responsibilities

* Lead and manage a growing People Operations team responsible for managing the employee experience throughout the employee lifecycle.
* Help shape a global People Operations strategy that aligns and moves the business towards continued growth, innovation and improvement.
* Provide HR support and consultation to the business; answering employee and manager questions about HR programs, policies, and other HR-related items.
* Provide HR support and leadership with respect to day-to-day HR life cycle activities of client groups, including performance management issues, investigations, and reorganizations.
* Understand workforce needs as the company scales managing costs while staying competitive with salary, benefits, perks, leaves, etc.
* Work with the executive team to come up with a competitive compensation strategy.
* Assess and apply market data, ensuring alignment with the company’s total compensation philosophy.
* Oversee performance, development, and the compensation review process.
* Drive a progressive, proactive, positive culture, and increase levels of engagement, enablement, and retention.
* Coach, influence, and provide guidance to business partners to figure out optimal organizational design, development, and performance management plans.
* Assist with the maintenance and accuracy of HR data, including processing SAP data, such as Employee Action Forms.
* Provide resolution to employee and organizational issues in a proactive and sensitive manner.
* Assess/identify HR strategy, policy, or process improvements.
* Ensure People Operations strategies and processes remain aligned with the company’s talent management and workforce plans to enhance employee engagement and sustain business growth.
* Collaborate with Chief People Officer, People Operations, and organizational leaders in fostering a culture of empowerment and performance; establish the employee lifecycle journey and succession plans.
* Maintain in-depth knowledge of local, global, and federal employment laws; maintain and store records judiciously and securely.

### Requirements

* 8+ years of progressive experience in HR roles with a demonstrable track record of building and optimizing processes, systems, and structures.
* Requires a Bachelor’s degree preferably in Human Resources, Organizational Development, Organizational Leadership, or related field.
* 5+ years hands-on experience with compensation & benefits.
* Labor Relations experience required, preferably in multiple countries.
* Working knowledge of regulatory and legal requirements related to total rewards and systems.
* 3-5 years of experience leading people and cross-functional organizations.
* Demonstrable ability to own, execute and deliver on short- and long-term projects.
* Strategic and innovative thinker; able to prioritize and use sound judgment and decision-making.
* Executive presence with excellent written and oral communication skills.
* Strong business insight and high EQ to successfully collaborate with executives and business partners at all levels.
* Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks).
* You share our [values](/handbook/values), and work in accordance with those values.
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#director-group)
* Ability to use GitLab

### Career Ladder

The next step in the People Operations job family is to move to the [People Leadership job family](/job-families/people-ops/people-leadership/). 
