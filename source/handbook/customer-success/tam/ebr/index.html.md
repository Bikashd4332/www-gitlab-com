---
layout: handbook-page-toc
title: "Executive Business Reviews (EBRs)"
---

# Technical Account Management Handbook
{:.no_toc}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

- [Technical Account Manager Summary](/handbook/customer-success/tam/)
- [CS Top 10](/handbook/customer-success/tam/cs-top-10/)
- [Capturing Customer Interest in GitLab Issues](/handbook/customer-success/tam/customer-issue-interest/)
- [Executive Business Reviews (EBRs)](/handbook/customer-success/tam/ebr/) *(Current)*
- [Account Engagement](/handbook/customer-success/tam/engagement/)
- [Escalation Process](/handbook/customer-success/tam/escalations/)
- [Customer Health Scores](/handbook/customer-success/tam/health-scores/)
- [Account Onboarding](/handbook/customer-success/tam/onboarding/)
- [Customer Renewal Tracking](/handbook/customer-success/tam/renewals/)
- [Account Triage](/handbook/customer-success/tam/triage/)

### Related Pages

- [Using Salesforce within Customer Success](/handbook/customer-success/using-salesforce-within-customer-success/)
- [Responsibility Matrix and Transitions](/handbook/customer-success/#responsibility-matrix-and-transitions/)
- [Customer Success & Market Segmentation](/handbook/customer-success/#customer-success--market-segmentation/)
- [Customer Success Vision](/handbook/customer-success/vision/)
- [Support handbook](/handbook/support/)
- [Sales handbook](/handbook/sales/)

---

## What is an Executive Business Review?

An Executive Business Review (EBR) is a strategic meeting with stakeholders from both GitLab and the customer. TAMs are responsible for scheduling and conducting EBRs and working with their customers to achieve the primary objectives.

TAMs should hold EBRs with each of their customers **at least** once per year, but more frequently is better to ensure the customer continues to engage and see the value of GitLab; however, this can vary depending on the needs of the customer.

The purpose of the EBR is to demonstrate to the [Economic Buyer](https://about.gitlab.com/handbook/sales/#capturing-meddpicc-questions-for-deeper-qualification) the value they are getting out of GitLab. It is meant to be interactive from both sides, discussing the usage of GitLab, how the customer has been using it, how it aligns with their business goals, and more. EBRs should not be treated as a sales call or an opportunity to up-sell, but rather to show that GitLab was a worthwhile investment and how we are working together as partners.

EBRs typically consist of the following content, but this list is only a guideline and should be tailored to the needs of each customer.

* Introductions
* Overview of GitLab & our Customer Success
* Product Roadmap
* Year in Review (growth, usage, etc.)
* Past & Future Business Goals
* Support Review
* Delivered Enhancements
* Relationship Building
* Q&A

For some pointers on the logistics of preparing for an EBR, please view our "[EBR in a Box](https://docs.google.com/presentation/d/1V3wzIZ9j6pVUbXpSeJgA_Lk-97C7_vr8NUJWk4J0__s/edit?usp=sharing)" presentation (internal to GitLab), which contains a suggested timeline, how to propose the EBR to the customer, how to prepare the content, and tips on presenting. There are also several example EBR decks and past recordings linked in this presentation for TAMs and other GitLabbers to review (please keep all customer-specific content confidential).
