---
layout: handbook-page-toc
title: "GitLab Offboarding"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

Should a new hire never start on their first day or they let us know they will not be starting before or soon after their first day that they will not be joining, this is not seen as an offboarding.
Instead they should be deleted entirely from BambooHR.
The People Experience Associate will inform the [team member with ownership rights](https://about.gitlab.com/job-families/people-ops/people-operations/#director-global-people-operations) of BambooHR to delete this profile entirely.

## Voluntary Offboarding

A voluntary offboarding occurs when a team member informs his or her manager of a resignation. The choice to leave GitLab was their decision.

If you are a current team member and you are thinking about resigning from GitLab, we encourage you to speak with your manager, your assigned PBP, or another trusted team member to discuss your reasons for wanting to leave.
At GitLab we want to ensure that all issues team members are facing are discussed and resolved before a resignation decision has been made.  This will allow GitLab the ability to address concerns and in return foster a great work environment.

If resignation is the only solution after you have discussed your concerns, then please follow these procedures.

### Procedures

1. Team members are requested to provide an agreed upon notice of their intention to separate from the company to allow a reasonable amount of time to transfer ongoing workloads.
1. The team member should provide a written resignation letter or email notification to their manager.
The PBP will acknowledge the reciept of the resignation with the team member confirming the last working day.
An [example email](https://docs.google.com/document/d/1rZvczqEuyyFDAzjheiF4LpcLuvj9fFd6maPOSDwkYpQ/edit) 
1. The PBP will also inform the team member that they will receive and invitation in the next 48 hours from CultureAmp to complete an exit survey.
1. Upon receipt of the resignation, the manager will notify the People Business Partner (PBP) by sending a copy of the resignation email/letter.
A discussion with the  manager and PBP should also happen if needed to determine what led up to the resignation.
1. PBP will forward the resignation email to the People Experience team email inbox as well as to the payroll lead.
If the team member has a contract with a co-employer, the payroll lead will forward the email to the contact at the co-employer.
1. The People Experience Team will organize the offboarding, depending on the location of the team member it may mean that another member of the People Experience team handles the offboarding.
This can be done by one of the People Experience Associates.
Collectively known as "People Exp" for the purposes of the following points:
      1. The PBP will post a message in the Slack #terms confidential channel and provide the name, last day, reason for resignation, rehire eligibility and location of the individual. 
      The Security lead and Payroll and Payments Lead should be `@`in the #terms channel.
      1. The PBP will assign the exit survey in Cultureamp for the departing team member to complete. 
      The PBP logs into Cultureamp and clicks on Surveys from the top menu bar. 
      The PBP will then select exit survey and selects the "start" exit button - this will allow the PBP to assign the survey to the departing team member. 
      The PBP then either selects themselves or another PBP to complete the exit interview.
1. If the individual has a statutory holiday allowance, the holiday taken should be confirmed with the manager and team member via email and then filed in BambooHR.
Once that has been done an acknowledgement letter can be prepared.
This will depend on the location and employment status of the team member.
Examples of these can be found here:
 - [GitLab Ltd (UK) Resignation Acknowledgement](https://docs.google.com/document/d/1zV1qnZmjQaNZ3QUjrLOtod-FbboNCPmYdqCLIAc7aos/edit)
 - [GitLab BV (Netherlands) Resignation Acknowledgement](https://docs.google.com/document/d/1-9hCL2Xs5po4lZ19L2vrcmGxQIYRD-Emyci3F63kt0c/edit)
1. For GitLab UK team members, payroll can pay up to and including the 5th day of the following month.
For example: If someone's last day is February 2nd, the team member can receive their final pay for this in January's payroll.
1. Once the acknowledgement email or letter is sent, the PBP will send the exit survey to the team member to complete.
1. People Experience Associate will create an [offboarding issue](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/offboarding.md) on or before the last day.

### Exit Survey

The exit survey provides team members with the opportunity to freely express views about working at GitLab.
People Operations will send the [CultureAmp](https://www.cultureamp.com/) survey to all team members who are leaving voluntarily.
The PBP & the team member may proactively set up time to discuss their responses and ask for further information.
Exit interviews are not mandatory, however your input will help provide GitLab with information regarding what is working well and what can be improved.

### GitLab Alumni Program

All offboarding may request to be added to the Slack channel `gitlab-alumni` by requesting through the PBP who can review the request and confirm to the People Experience Associate who can open an [access request](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=slack_googlegroup_1Passwordgroupvault) on behalf of the former team member.
Reasons that someone would not be permitted to join would be due to involuntary offboarding due to extreme behavior or gross conduct.
Underperformance is not a reason to exclude someone from from the channel.
The purpose of this channel is to network and socialize with team members.
Joining the channel is voluntary and subject to GitLab's [Code of Conduct](/community/contribute/code-of-conduct/).

GitLab, the company, monitors the channel and can remove people from it at their sole discretion.
The GitLab [code of conduct](https://about.gitlab.com/community/contribute/code-of-conduct/) is enforced in the channel.

## Involuntary Offboarding

Involuntary offboarding of any team member is never easy.
We've created some guidelines and information to make this process as humane as we can.
Beyond the points outlined below, make sure to refer to our guidelines on [underperformance](/handbook/underperformance), as well as the [offboarding issue](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/offboarding.md).

## Leave of Absence or Garden Leave

In some cases a team member will go on a Leave of Absence or Garden Leave prior to their actual offboarding date from GitLab.  Team members on LOA or Garden leave will have no access to GitLab systems and are not required to do any work on GitLab's behalf.  Once the LOA or Garden leave expires the team member will be officially offboarded from GitLab.  Prior to the offboarding issue and the overall process for the term listed below.  The PBP will complete the following:

*  PBP will have a legal/CPO review of the planned offboarding.
*  PBP will inform payroll, compensation and benefits, security and the stock administration of the date the team member will have access suspended and the official offboarding date prior to the start of the official offboarding issue.  The PBP can either inform the group via the confidential #terms channel 1-2 days prior to the scheduled exit or via a zoom call.  
*  PBP will work with the People Operations Manager to ensure the offboarding issue has the correct dates and all GitLab offboarding team members in payroll, compensation and benefits, security and stock administration have been communicated to and understand the correct offboarding date. 
*  The offboarding process will remain the same as listed below in the Overall Process.  The PBP will notify the team via  the confidential #terms channel to start to start turning off the team members access.  

### Overall process

Ideally, the manager and the team member have walked through the guidelines on [underperformance](/handbook/underperformance) before reaching this point.

1. Manager: Reach out to the appropriate People Business Partner (PBP) for assistance.
PBP will ask about what the performance issues have been, how they have been attempted to be addressed and review all mamager/team member documentation.
Once the review has been completed and the decision has been made to terminate the team member the PBP will coordinate the offboarding of the team member.
1. If the team member is employed with a co-employer, that co-employer should be consulted as they can provide advice on the appropriate procedure to follow.
1. PBP: Will create a private slack channel that will include the PBP, Manager and Leader of the organization to review the offboarding and agreed upon offboarding date. 
1. Manager: Once the date and time is confirmed they send the PBP a private and separate calendar invite with the zoom details for the meeting with the team member. 
1. PBP: Will tag the People Experience team, security team and payroll via the confidential #terms channel but depending on the situation it may be necessary to discuss this via direct message or video call. The PBP will confirm and coordinate the date and time of the offboarding and who will be available to assist with the offboarding.
1. PBP: Will send a private calendar request to the identified people experience, security and payroll lead blocking time on calendar when the offboarding will begin.  Once the conversation has started the PBP will slack the team members in the confidential #terms channel with the name of the team member to immediately start the offboarding process.
1. PBP: Will coordinate with the manager and/or CPO to determine if a [separation and release of claims agreement](#Separation-and-Release-of-Claims-Agreements) is appropriate. If so, have it prepared prior to the call.
1. Manager and PBP: Discuss best mode of communicating the bad news to the team member. This discussion can happen via a private chat-channel, but it is best to be done via video.
1. Manager and PBP: Decide who will handle which part of the conversation, and if desired, practice it.
If needed the PBP will provide the manager with a script for the offboarding meeting.
1. Manager and PBP: If the team member who is being terminated is a people manager a communication plan for the team regarding the departure should be in place before the offboarding proceeds.
The plan should include identification of an interim leader if possible, scheduled meeting with interim leader, a scheduled team call to announce the departure and the interim leadership, and then an announcement on the company call.
Informing the team and answering questions should be the top priority.
No announcement should be made on a company call until a team call has been completed.
In most cases a team call can occur the same day of the offboarding, if necessary the offboarding can be announced on the company call the following day.
1. Manager and PBP: Decide what offboarding actions need to be taken _before_ the call (e.g. revoke admin permissions), or _during_ the call (e.g. revoke Slack and Gmail access), and which ones can wait until later (see the [offboarding issue](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/offboarding.md) for the full list of actions).
Do not create the offboarding issue until _after_ the call, since even confidential issues are still visible to anyone in the team.
If the team member is a risk to the production environment the PBP should privately reach out to the Infrastructure Managers to determine who will be available to assist with the offboarding.
Once an infrastructure resource has been identified they should be added to the private calendar invite sent to people experience, security and payroll to hold the time for the team member offboarding.
Once the offboarding conversation starts the PBP will slack the infrastructure contact the name to start the offboarding process.
1. Manager: Set up a call with the team member in question.
Make a separate private calendar event to invite the PBP.
1. On the call: Deliver the bad news up-front, do not beat around the bush and prolong the inevitable pain for everyone involved.
A sample leading sentence can be "Thanks for joining the call, ____ .
Unfortunately, the reason I wanted to speak with you is because we have decided that we have to let you go and end your employment / contract with GitLab."
At this point, hand over the call to PBP to continue.
The Manager will make it clear that the decision is final, but will also explain what led to this decision and will point to the process that was followed to reach this decision.
PBP will also make it clear that the decision is final, but also will genuinely listen to their side of the story since there may be useful lessons in what they say for the rest of the team e.g. regarding hiring and vetting practices.
1. PBP: Make sure to communicate the [practical points](#offboarding-points) from the offboarding memo outlined below.
1. PBP: Once the conversation is complete upload the severance document into HelloSign for review and signing.
1. People Experience: Create the [offboarding issue](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/offboarding.md) and complete all relevant tasks.
1. Payroll: If an employee, review if any time off needs to be paid on the last paycheck by looking in BambooHR under Time Off.
1. People Operations: If applicable, sign the team member up for a [Mentat](https://thementat.com/) subscription. Credentials, licenses, and pricing are in the People Ops 1Password vault.
1. People Operations: If appropriate (default is that it is appropriate), send a [offboarding memo](#offboarding-memo).

### Points to cover during the offboarding call, with sample wording
{: #offboarding-points}

The following points need to be covered for any team member:
1. Final Pay: "Your final check (or invoice period) is for the pay period of X and includes X days of pay”.
1. Company Property: “Please return all property as explained in the handbook, also please delete GitLab’s email connection from your phone”.
1. Business Expenses: “Please create your final expense report to Expensify (for employees), OR, please file any outstanding expenses with your final invoice (for contractors), so these can be reimbursed to you in a timely manner”.
1. Confidentiality and Non-Disclosure: “We know you are a professional, please keep in mind the agreement you signed when you were hired”.
1. If you would like GitLab to share your personal email with the rest of the company, please send an email to People Ops or a farewell message that can be forwarded on your behalf.

The following points need to be covered for US-based employees:
1. COBRA: “Your benefits will cease on last day of the month you are eligible for Consolidated Omnibus Budget Reconciliation Act (“COBRA”), the carrier (Lumity) has been notified and the carrier will send out the paperwork to your home address on file”.
1. PPACA: "You may also be eligible under the Patient Protection and Affordable Care Act (“PPACA”) for subsidized health care options via the marketplace.
If you are interested it is important that you sign up with the market place well before the 15th of the month to have coverage for the following month”.
1. HIPAA: " Under the Health Insurance Portability and Accountability Act of 1996 (HIPAA), if you need a certificate of credible coverage please download it from your current carrier's online portal or request it from People Ops”.
1. Unemployment insurance: "It is up to your state's labor agency (in CA: EDD) to decide if you are eligible for unemployment insurance”.
1. Please remember to keep GitLab informed: "If you move I want to be sure your W-2 gets to you at the end of the year.
You may also contact X at GitLab (provide phone number and email address) with any other questions that you may have" (consider inviting them to contact you at anytime for any reason).

### Sample offboarding memo

If appropriate (to be determined by conversation with the manager, the Group Executive, and People Ops), use the following [offboarding memo](https://docs.google.com/document/d/11Uk8p4VJrLnDD5IAtbTwswPUUEnmeEOazS1kJMhOu70/edit?usp=sharing), which is provided here as an openly viewable Google Doc, but of course needs to be personalized and tailored to each individual's situation.
As written, it is applicable to US-based employees only.

### Separation and Release of Claims Agreements
If appropriate (currently only in the case of US-based employees, and to be determined by conversation with the manager, the Group Executive, and People Ops) prepare a Separation and Release of Claims Agreement following the steps outlined here.
All of these steps are done by People Ops unless specified differently.
1. Prepare the agreement using the standard template.
1. Have the legal team review the agreement.
1. Additional notes:
   1. Separation pay is not paid until the ex-team member signs the document and the revocation period has passed.
   1. Make sure you understand the rules for over 40.
   1. You must use language in your exit meeting that is in no way forceful of the ex-team member to sign the agreement. Use phrasing such as “if you choose to sign”; “you have a right to have legal council review this document before you sign”, etc.

## Departures are unpleasant

As explained briefly in the [offboarding issue](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/offboarding.md), GitLab does not provide any context around why people are leaving when they do.
However as mentioned in the [procedures](/handbook/offboarding/#procedures), for voluntary offboarding, the team member can share their reasons for leaving if they wish.
If they choose not to then we say:  
> "As of today, X is no longer with GitLab. I would like to take this opportunity to thank X for their contributions and wish them all the best for the future.
> If you have questions about tasks or projects that need to be picked up, please let me know.
> If you have concerns, please bring them up with your manager."

If someone is let go involuntarily, this generally cannot be shared since it affects the individual's privacy and job performance is intentionally kept [between an
individual and their manager](/handbook/communication/#not-public).
Unless it's for extreme behaviour/gross misconduct, the team member will be advised the areas of concern about their performance, given support and guidance on how to improve and provided with an appropriate timeframe to reach and sustain the expectations.

If you are not close to an employee's offboarding, it may seem unnecessarily swift.
Please remember that these decisions are never made without following the above process to come to a positive resolution first - we need to protect the interests of the individual as well as the company, and offboarding are a last resort.
According to our values [negative feedback is 1-1 between you and your manager](/handbook/values/#negative-is-1-1) and we are limited in what we can share about private employee issues.
Please discuss any concerns you have about another employee's offboarding with your manager or your People Business Partner.

Given the expectations and responsibility that come with a VP and above position, when there is an involuntary offboarding for one of these positions, additional context for the personnel change can be provided to the organization.  

Silence is unpleasant. It's unpleasant because we are human, which means that we are generally curious and genuinely interested in the well-being of our team members.

Is it _more_ unpleasant in a remote setting? Probably not.
When people are all in the same office building, they can "kinda sorta" know what may be coming because of the grapevine, the water cooler, and so on. When the news hits it might be less of a shock - only because of unprofessional behavior in the first place.
But at larger companies with multiple office buildings, departures will tend to come as more of a surprise and with less context (at least to the people in other buildings).

## Turnover Data

GitLab's [turnover data](https://docs.google.com/a/gitlab.com/spreadsheets/d/1yk_tlu-Qy3j4VbaB8Yt4wMc5Gocxri9cp7F57DVWyYM/edit?usp=sharing) is only viewable internally.
This data is updated on a monthly basis by People Operations.

### Managing the Offboarding Tasks
#### Offboarding Issue
To track all tool deprovisioning, please open an offboarding issue following the [offboarding guidelines](https://about.gitlab.com/handbook/offboarding/offboarding_guidelines/).

#### Returning property to GitLab
{: #returning-property}

As part of offboarding, any GitLab property valued above 1,000 USD needs to be returned to GitLab.
One exception to this rule is that the team member can opt to purchase their laptop at a depreciated market price which will be provided by Finance upon request.

To return your laptop to GitLab, please contact itops@gitlab.com immediately upon offboarding.

#### Expensify

To remove someone from Expensify Log in to [Expensify](https://www.expensify.com/signin) and go to "Settings" in the left sidebar.
Select the right policy based upon the entity that employs the team member. Select "People" in the left menu.
Select the individual's name and click "Remove".
If the person has a company credit card assigned to them please notify Finance before un-assigning it.

### Evaluation

1. How could this outcome have been avoided?
2. Were there early signs that were missed?
3. In retrospect, what questions should have been asked to bring awareness and
  ownership to performance issues?
For example, "How would you compare yourself relative to your peers?"
People are surprisingly honest here.

## Announcing Offboarding

We strive to maintain personal information regarding all team members private, this includes information regarding a team members voluntary or involuntary departure from GitLab.
However, a manager with the consent and approval of the departing team member can share more details with the GitLab team regarding the decision to leave GitLab.

For a voluntary departure a team member may have chosen to leave for many different reasons, career development, promotion, a new role or career path, dislike remote work, etc.
For example, a team member may tell their manager that they really miss being in an office environment and remote work is not suitable for their personality.
Based on that decision they have taken another opportunity that allows them to go to an office.

If the departing team member gives their manager permission to share that information then the manager will share while making the departure announcement on the team call.
We want all GitLab team-members to be engaged, happy and fulfilled in their work and if remote work, the requirements of the job or the role it self are not fulfilling we wish our team members the best of luck in their next endeavor.

Regarding involuntary offboarding, certain information can also be shared with the GitLab team regarding the departure.
Some team members do not thrive or enjoy the work that they were hired to do.
For example after starting at GitLab a team member quickly learns they have no desire or interest to learn Git or work with Git.  This doesn't make them a bad person, it just means they don't have the interest for the role and based on that the decision was made to exit the company.
Again, we want all team members to enjoy and thrive in their work and that may or may not be at GitLab.

The departing team member may author a goodbye message for either voluntary or involuntary offboarding:
  1. Write your message, for example, "It was a pleasure working with you all.
  I've decided to pursue an opportunity in a different industry that is more aligned with my interests.
  I'll be rooting for GitLab from the sidelines.
  Stay in touch: Cheers!"
  1. Send it to your manager and People Partner for approval.
  1. The manager can with the permission of the team member post the agreed upon message verbatim as long as the message is deemed appropriate in the company call agenda and slack goodbye announcements.
  1. If appropriate, managers are encouraged to thank departing team members for their contributions and wish them the best of luck on their future endeavors.

In some instances there will be no further clarification on why a team member has departed, if there are concerns you can address those with your manager.
Different levels of transparency will exist based on maintaining respectful treatment for all departures.
Having team members leave may be a learning opportunity for some, but should not be a point of gossip for anyone.
Managers will need to balance the opportunity for learning with the expectation of privacy and consult their People Business Partner should they have questions.

Transparency is one of our values.
In the case of offboarding transparency [can be painfully specific, calling out an employee’s flaws, while inviting more questions and gossip](https://outline.com/PTGkER).
We opt to share the feedback only with peers and reports of the person since we balance transparency with our value of collaboration and negative is 1-1.
