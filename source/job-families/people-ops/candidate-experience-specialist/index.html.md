---
layout: job_family_page
title: "Candidate Experience Specialist"
---

## Candidate Experience Specialist

The GitLab Candidate Experience Specialists work to create positive experiences for GitLab candidates and hiring teams. In a growing fast-paced environment, the Candidate Experience Specialist is a dynamic team member who helps create an amazing candidate experience, improve our existing hiring practices, and deliver exceptional results to our hiring teams. GitLab strives to be a preferred employer and relies on the Candidate Experience Specialists to act as brand ambassadors by embodying the company values during every interaction with potential team members. The Candidate Experience Specialist is enthusiastic about working with high volume and is dedicated to helping GitLab build a qualified, diverse, and motivated team. The Candidate Experience Specialist reports to the Manager, Candidate Experience.

### Responsibilities

* Collaborate with Recruiters, hiring teams and Recruiting leadership to understand and implement the hiring plan for each job requisition
* Coordinate with the Recruiters to maintain correct process information in the Applicant Tracking System (ATS) and GitLab repositories
* Build and update an effective network of internal and external resources to call on as needed
* Ensure candidates receive timely, thoughtful and engaging messaging throughout the hiring process
* Work in Applicant Tracking System (ATS) to help recruiters maintain a positive candidate experience for candidates
* Helps manage candidate traffic for all roles
* Act as the [Recruiting Coordinator](https://about.gitlab.com/handbook/hiring/recruiting-framework/coordinator/) for the interview process
* Assist with conducting reference checks, if applicable, for all candidates entering the final round of the interview phase
* Create and stage employment contracts for candidates receiving an offer of employment on behalf of the hiring manager and recruiter
* Work to create a seamless handoff of new team members to the [People Operations Specialist](https://about.gitlab.com/job-families/people-ops/people-operations/) team
* Promote our values, culture and remote only passion

### Requirements

* Desire to learn about Talent Acquisition and GitLab from the ground level
* Demonstrated ability to work in a team environment and work with C-Suite level candidates with a focus on delivering an excellent candidate experience
* Ambitious, efficient and stable under tight deadlines and competing priorities
* Proficient in Google Suite
* Ability to build relationships with team members, hiring managers and colleagues across multiple disciplines and timezones
* Outstanding written and verbal communication skills across all levels
* Willingness to learn and use software tools including Git and GitLab
* Organized, efficient, and proactive with a keen sense of urgency
* Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks)
* Excellent communication and interpersonal skills
* Prior experience using an applicant tracking system, Greenhouse experience is a plus
* Ability to recognize and appropriately handle highly sensitive and confidential information
* Experience working remotely is a plus
* Share our [values](/handbook/values) and work in accordance with those values
* Ability to use GitLab

## Senior Candidate Experience Specialist

Senior Candidate Experience Specialist shares the same requirements and responsibilities as the Intermediate Candidate Experience Specialist with the addition of the following:

### Responsibilities

* Serve as a mentor to the rest of the team through leading by example, sharing best practices, and a resource for questions and guidance
* Serve as an example of dogfooding Gitlab.com and assist team members with questions
* Provide insight and feedback for process improvement opportunities
* Lead by example and performance with the team OKR’s and Performance Indicators
* Serve as a subject matter expert
* Consistently provide a great candidate experience by teaching others how to embody the GitLab values

### Requirements

* 3-5 years experience in recruiting/HR/People
* Consistent track record and strong performance
* Experience working directly with hiring teams
* High sense of urgency
* Proven organizational skills with high attention to detail and the ability to prioritize
* Confidence to learn new technologies and translate that learning to others
* Experience as mentor, guide, or subject matter expert

## Performance Indicators

*   [Average candidate ISAT](https://about.gitlab.com/handbook/hiring/metrics/#interviewee-satisfaction-isat)
*   [Hires vs. Plan](https://about.gitlab.com/handbook/hiring/metrics/#hires-vs-plan)
*   [Time to Offer Accept](https://about.gitlab.com/handbook/hiring/metrics/#time-to-offer-accept-days)
*   [CES Service Desk Issues Response Time and Issue Distribution](https://about.gitlab.com/handbook/hiring/metrics/#ces-service-desk-metrics)
*   [CES Response Time](https://about.gitlab.com/handbook/hiring/metrics/#ces-response-time)
*   [Candidate Time Per Stage](https://about.gitlab.com/handbook/hiring/metrics/#candidate-time-per-stage)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](https://about.gitlab.com/company/team/).

   * Qualified candidates will be invited to schedule a 30 minute screening call with a Recruiting Manager
   * Then, candidates will be invited to schedule two 30 minute interviews with two separate Peers and a 30 minute interview with another Recruiting Manager
   * Finally, candidates will be invited to a 45 minute interview with the Hiring Manager

As always, the interviews and screening call will be conducted via a [video call](https://about.gitlab.com/handbook/communication/#video-calls). See more details about our interview process [here](https://about.gitlab.com/handbook/hiring/interviewing/).
