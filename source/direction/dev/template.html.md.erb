---
layout: markdown_page
title: "Product Section Direction - Dev"
---

- TOC
{:toc}

Last Reviewed: 2020-03-04

![Dev Overview](/images/direction/dev/dev-overview.png)

<figure class="video_container">
    <iframe src="https://www.youtube.com/embed/mIpHEbyhsj0" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Dev Section Overview

The Dev Section is made up of the [Manage](/handbook/product/categories/#manage-stage), [Plan](/handbook/product/categories/#plan-stage), and [Create](/handbook/product/categories/#create-stage) stages of the DevOps lifecycle. The scope for the Dev section is wide and encompasses a number of analyst categories including Value Stream Management, Project Portfolio Management, Enterprise Agile Planning Tools, Source Code Management, IDEs, Design Management, and even ITSM. It is difficult to truly estimate TAM for the Dev Section, as our scope includes so many components from various industries, but research from IDC indicates the estimated [TAM](https://docs.google.com/spreadsheets/d/1HYi_l8v-wTE5-BUq_U29mm5aWNxnqjv5vltXdT4XllU/edit?usp=sharing) in 2019 is roughly ~$3B, growing to ~$7.5B in 2023 (26.5% CAGR). Alternatively, the Dev product management team has conducted a bottoms up Total Addressable and Servicable Addressible market analysis which estimates GitLab's SAM for Dev to be 19.5B in 2019 growing to 27.5B in 2023. Analysis: [Manage](https://docs.google.com/spreadsheets/d/1357Zwbf0yTjcFBuCdX2HnNLBaF9uWnW7Auv4F94aoSo/edit#gid=0) [Plan](https://docs.google.com/spreadsheets/d/1dDNSR_mE4peeOc_3Xhqm_VLoTyk4gSbcbiGfuMrOg6g/edit#gid=0) [Create](https://docs.google.com/spreadsheets/d/1KSwTVPIvMO8IXkMqA40Ms1rL-glmo-w7MjZvUrjMFGA/edit#gid=0)

Based on [DevOps tool revenue](https://drive.google.com/file/d/1VvnJ5Q5PJzPKZ_oYBHGNuc6D7mtMmIZ_/view) at the end of 2018 and comparing to GitLab annual recurring revenue at the end of FY20 Q3, our estimated market share is approximately 1.5% based on revenue. (Note: this assumes we can attribute 100% of GitLab revenue to Dev stages.) Market share based on source code management is somewhere in the [30%](https://docs.google.com/document/d/15TLEUc9BxiiB9N33MW-7zGvfitfFXQj3TM8BfM2q4hM/edit?usp=sharing) range.

Nearly [half of organizations](https://drive.google.com/file/d/17ZSI2hGg3RK168KHktFOiyyzRA93uMbd/view?usp=sharing) still have not adopted DevOps methodologies, despite [data](https://drive.google.com/file/d/17MNecg84AepxWlSDB5HjNBrCJggaS9tP/view?usp=sharing) that indicates far higher revenue growth for organizations that do so. Migrating a code base to a modern, Git-backed source control platform like GitLab can often be the first step in a DevOps transformation. As such, we must provide industry-leading solutions in source code and code review, as this is not only the entry into DevOps for our customers, but typically the entry into the GitLab platform. Once a user has begun utilizing repositories and code review features like Merge Requests, they often move “left” and “right” to explore and utilize other capabilities in GitLab, such as CI and project management features.

Per our [Stage Monthly Active User data](https://app.periscopedata.com/app/gitlab/604621/GitLab.com-SMAU) we know that Create has the highest usage amongst GitLab stages, alongside Manage, which all users in groups uses. As such, these stages must focus on security fixes, bug fixes, performance improvements, UX improvements, and depth more than other areas of GitLab. Plan, while introduced in 2011 with the release of issue tracking, still falls far behind market leaders who have better experiences for sprint management, portfolio management, roadmapping, and workflows.

Other areas, such as Value Stream Management are nascent to both GitLab and the market, and will require more time devoted to executing problem and solution validation discovery. 

Over the next year, Dev will require focus on both breadth and depth activities, and each stage will require significant investment to accelerate the delivery of security issues, performance issues, and direction items. 

Within each stage, the listed items in the 1-year plan are in **order of priority**. The top priority for each stage is:

* Manage: [Enterprise Readiness](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=Enterprise%20Readiness)
* Plan: [Importing from Jira](https://gitlab.com/groups/gitlab-org/-/epics/2586)
* Create: [High Availability Git](https://gitlab.com/groups/gitlab-org/-/epics/842)

## 1-Year Plan: What’s Next for Dev

Over the next 12 months, each stage in the Dev section will play an integral part in this strategy.

Please see the [categories page](/handbook/product/categories/#dev-section) for a more detailed look at Dev's plan by exploring `Direction` links in areas of interest.

<%= partial("direction/dev/plans/manage") %>

<%= partial("direction/dev/plans/plan") %>

<%= partial("direction/dev/plans/create") %>

### Themes that cross all Dev stages
**Performance and availability:** We must invest in the performance, stability, and availability of our application. We will do this by focusing on [application limits](https://gitlab.com/groups/gitlab-org/-/epics/1737#note_202179305), [diff load times](https://gitlab.com/groups/gitlab-org/-/epics/1816), and ensuring [availability](https://about.gitlab.com/handbook/product/product-management/process/#prioritization) is top of mind.

Growth driver: Retention

### What we're not doing next year

Choosing to invest in the above areas in 2020 means we will choose not to:
* DevOps for ML/AI: Treat ML models as first-class citizens in GitLab. Instead, we will focus on getting large assets to become performant via improvements to Gitaly. Once this is completed, we will focus on ML models.
* Efficiency recommendations: Provide recommendations where customers can improve their efficiency in the DevOps lifecycle. This will likely require comparisons amongst many GitLab users and an AI engine to make intelligent recommendations. We won't be working on features that help companies answer, “Am I doing the right activities?” These improvements will come in years two and three of the VSM plan.
* Chasing feature parity: Measuring success against achieving feature parity with our competitors. While we strive for making it easy for companies to migrate to GitLab, we realize that we need to provide *substantially more value* compared to other solutions for our current and prospective customers. 
* Container based IDE: Investing into a container based IDE solution bundled with GitLab to replace the existing Web IDE.
* Feature objects: Creating first class feature objects per the project management morphs into product management block above. While we think this is important, we must spend this year building a better foundation in portfolio management.
* Wikis: Investments in wikis, such as WYSIWYG editing, beyond markdown rendering improvements and group wikis.
* Web IDE for mobile: Instead, we'll utilize the Static Site Editor group to provide a mobile editing experience. The Web IDE will still develop for a great iPad experience.

## 3 Year Dev Section Themes

Our direction for the Dev section is to provide the world’s best product creation and management platform.
We believe we have a massive opportunity to change how cross-functional, multi-level teams collaborate by
providng a solution that breaks down organizational silos and enables more effective, efficient value delivery.
We want to provide a solution that enables higher-quality products to be more quickly iterated upon. We also want to to make it effortless for companies to migrate to GitLab. In order to obtain adoption at scale, GitLab has to provide *substantially more value*
than our competitors. Additionally, we believe the majority of value can likely be delivered by substantially fewer
features than our competition. The following themes listed below represent how we believe we will deliver this value
and is our view of what will be important to the market and to GitLab over the next 3 to 5 years. As such,
they will be the cornerstone of our 3-year strategy, and all activities in the 1-year plan should advance
GitLab in one or more of these areas.

<%= partial("direction/dev/themes/automated_code_review") %>

<%= partial("direction/dev/themes/value_stream_measurement") %>

<%= partial("direction/dev/themes/more_devops_personas") %>

<%= partial("direction/dev/themes/enterprise_digital_transformation") %>

<%= partial("direction/dev/themes/project_to_product") %>

## 3-Year Strategy

In three years, the Dev Section market will:

* Centralize around Git as the version control of choice for not only code, but for design assets, gaming, silicon designs, and AI/ML models.
* Have a market leader emerge in the value stream management space. Currently, the market is fragmented with most players focused on integrations into various DevOps tools.
* Adopt a mindset shift from project management to product management.
* Recognize the value of a single platform for all software creation activities, including product management.
* See an uptick in startups and applications being built on the backs of a "no code" framework

As a result, in three years, GitLab will:

* Provide a next-generation, highly performant Git-backed version control system for large assets, such as ML models. Our goal in three years should be to host the most repositories of these non-code assets.
* Emerge as the leader in VSM and be recognized in the industry by customers and analysts as such. Our goal in three years should be to provide the best insights into the product development process that no other tool can come close to, as we have a [unified data model](https://www.ca.com/en/blog-itom/what-is-a-unified-data-model-and-why-would-you-use-it.html) due to GitLab being a single platform.
* Develop an industry-leading product management platform where multiple features and products can be measured and managed easily.
* Research and potentially add capabilities for "no code" workflows



## Stages & Categories

<%= partial("direction/dev/strategies/manage") %>

<%= partial("direction/dev/strategies/plan") %>

<%= partial("direction/dev/strategies/create") %>


## What's Next 

<%= direction["all"]["all"] %>
