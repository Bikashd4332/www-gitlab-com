---
layout: handbook-page-toc
title: "Business Operations Offsites"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## Business Operations Offsites

### Fast Boot 2019-12-10; Washington, DC, United States, Earth
The Business Operations team took part in a one day [Fast Boot](/handbook/engineering/fast-boot/). This [planning issue](https://gitlab.com/gitlab-com/business-ops/Business-Operations/issues/194) contains the proposal and this page documents the discussions and outcomes.

#### Details
*  Where: Washington DC
*  When: December 10th (arrive the 9th, leave the 11th)
*  What: Planning
*  Why: only 5 of us (16..17? and counting) were in NOLA for contribute -- as a support org, we are bringing ourselves together to better prepare to support the business as we come into FY21. This timeline will be after initial planning, and before we finalize, allowing us to better plan and respond
*  How: Please be frugal.
*  Ambassadors: Walter Zabaglio and Taylor Murphy
*  [Initial Issue for location and time details](https://gitlab.com/gitlab-com/business-ops/Business-Operations/issues/194)
*  [Issue for planning the event](https://gitlab.com/gitlab-com/business-ops/Business-Operations/issues/206)
*  [Issue for Topics/Agenda](https://gitlab.com/gitlab-com/business-ops/Business-Operations/issues/213)


##### TBD
*  To Be Scheduled: Pre-Planning Event (plan to plan)
*  To Be Scheduled: Retrospective (how can we do this more

##### Questions?
*  Is there a per person budget?
  *  Please be Frugal. Your travel expenses should be easily covered by our visiting incentive [link]. We can expense 2 nights of hotels. Feel free to do anything else you'd like, but that's all we're covering. We will likely plan a team meal.
* What's an ambassador?
  * Great question!

#### Key Outcomes Planned

#### Deep Dives

#### Outcomes

