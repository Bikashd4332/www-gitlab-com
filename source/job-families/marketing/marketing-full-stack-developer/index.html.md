---
layout: job_family_page
title: "Website Full Stack Developer"
---

As the Website Full Stack Developer you will work closely with design, UX, product marketing, content marketing, and other members of the Marketing team. This role will be part of the Marketing Website team and report directly to the Manager, Product Marketing, Ops

## Responsibilities

* Lead architecture and engineering (Ruby, Middleman, Haml) of [about.gitlab.com](/)
* Run the marketing website as an open source project, optimizing for maximum contributions to both code and content.
* Define back-end frameworks, code style guides, and templates to empower everyone to contribute.
* Implement site speed improvements and technical SEO.
* Own CI automation using GitLab to build, test, and deploy.

## Requirements

* 6+ years experience specializing in back-end development, website and web applications.
* Significant experience with Ruby.
* Expert-level understanding of responsive design and best practices.
* Strong working knowledge of HTML, CSS, and JavaScript (jQuery).
* You use research and best practices to create, validate, and present your ideas to project stakeholders.
* Able to iterate quickly and embrace feedback from many perspectives.
* Strong understanding of information architecture, interaction design, and user-centered design best practices.
* Understanding of Git and comfortable using the command line.
* A track record of being self-motivated and delivering on time.
* Candidates in Americas timezones will be preferred.
* Ability to use GitLab

### Nice-to-haves

* Previous experience with Static Site Generators like Middleman, Jekyll, Hugo, etc.
* Experience working in a fully or partially remote company.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Portfolios of qualified candidates will be reviewed by our hiring team
* Select candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* First, candidates will be invited to schedule a 45 minute interview with our Manager, Product Marketing, Ops
* Second, candidates will be invited to schedule a 45 minute interview with our Marketing Web Developer/Designer
* Third, candidates will be invited to schedule a 45 minute interview with one or more members of the Engineering team
* Fourth, candidates will be invited to schedule a 45 minute interview with our Director of Product Marketing
* Finally, successful candidates may be asked to interview with our CEO
